window.h5Host = "http://h5.easy-ai.com";
// window.h5Host = "http://192.168.1.11:63338/photo_ins_h5";

window.apiHost = "http://api.easy-ai.com/ins";

// window.apiHost = "http://192.168.1.11:8081/ins";


function resolveHtmlAndGetUserInfo(html, success, fail, error) {
  var businessParam = {
    "html": html
  };
  doPost2('/api/h5/resolveUserInfo', businessParam, success, fail, error);
}

function servicePrices(serviceType, success, fail, error) {
  var businessParam = {
    "serviceType": serviceType
  };
  doPost2('/api/h5/service_prices', businessParam, success, fail, error);
}

function getNotices(success, fail, error) {
  doPost2('/api/h5/notices', {}, success, fail, error);
}


function sendMsgToApp(data) {
  try {
    window.webkit.messageHandlers.PhotoApp.postMessage(data);
  } catch (e) {
    console.log(e);
    loading(false);
  }
}

function buyProdService(type, link, num, success, fail) {
  var serviceType;
  if ("likes" === type) {
    serviceType = 3;
  } else if ("followers" === type) {
    serviceType = 4;
  } else if ("views" === type) {
    serviceType = 5;
  }

  var uid = getUid(true);

  doPost2('/api/gold/buy_prod_service', {
    "uid": uid,
    "link": link,
    "quantity": num,
    "serviceType": serviceType
  }, function (data) {

    sendMsgToApp({
      'type':'updateCoin',
      "uid": uid,
    })

    showModel('Congratulations！You will add '+num+' '+type+' within 24 hours 🎉', {}, function (msg) {
      console.log(msg);
    }, function (data) {
      console.log(data);
    });

  }, function (msg) {
    if ("no enough coin" === msg) {
      sendMsgToApp({
        "type": "buyCoin",
        "uid":  getUid(true)
      });
      showToast(msg, 2);
    }
    fail(msg);
  });
}

function storeUid(uid) {
  sessionStorage.setItem('sirenbang-uid', uid);
}

function getUid(bixu) {
  var uid = sessionStorage.getItem('sirenbang-uid');
  if (bixu && !uid) {
    showToast('Please login first', 3);
    setTimeout(function () {
      goback();
    }, 3000)
  }
  return uid;
}


/**
 * 根据instName获取主页地址信息
 */
function getInstHomeInfo(instName, success, fail) {
  doPost2('/api/h5/getUserHomePageUrl', {
    "insName": instName
  }, success, fail);
}

/**
 * 通知app抓取html数据
 */
function notifyAppGetHtml(instName) {
  getInstHomeInfo(instName, function (data) {
    console.log('向app发送消息', data);
    sendMsgToApp(data)
  }, function (msg) {

  });
}

function keepOneDecimal(num) {
  if (isNaN(num)) {
    return num;
  }

  if (num <= 1000) {
    return num;
  }

  var result = parseFloat(num);
  result = result / 1000;
  result = Math.round(result * 10) / 10;
  return result + 'k';
}

function doPost2(api, businessParam, success, fail) {
  var url = window.apiHost + api;
  console.log("请求url:" + url, businessParam);


  businessParam['package_name'] = 'com.editor.photo';
  businessParam['platform'] = 'ios';
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": url,
    "method": "POST",
    "headers": {
      "Content-Type": "application/x-www-form-urlencoded",
      "cache-control": "no-cache"
    },
    "data": {
      "code": encrypt(JSON.stringify(businessParam))
    }
  }

  $.ajax(settings).done(function (result) {
    console.log("请求url:" + url, businessParam, "response:" + JSON.stringify(result));

    if (result.code !== '0') {
      fail(result.msg);
    } else if (result.code === '0') {
      success(result.data);
    }
  });
}

/**
 * 回退按钮
 */
function goback() {
  window.history.go(-1);
  window.location.reload();
}

function loading(show) {
  if (show) {
    $('body').append('<div class="loader" id="loading">Loading...</div>')
  } else {
    $('#loading').remove();
  }
}

(function ($) {
  $.getUrlParam = function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURIComponent(r[2].replace(/\+/g, '%20'));
    return null;
  }
})(jQuery);

function toThousands(num) {
  var result = [], counter = 0;
  num = (num || 0).toString().split('');
  for (var i = num.length - 1; i >= 0; i--) {
    counter++;
    result.unshift(num[i]);
    if (!(counter % 3) && i != 0) {
      result.unshift(',');
    }
  }
  return result.join('');
}

/**
 * 模态框
 */
function showModel(content, attachData, cancle, ok) {
  var container = document.getElementById('model-div-container');

  if (!attachData) {
    attachData = {};
  }

  //如果没有
  if (!container) {
    $('body').append("<div id=\"model-div-container\" style=\"\n" +
      "      display: none;\n" +
      "      position: fixed;\n" +
      "      top: 0;\n" +
      "      width: 100%;\n" +
      "      height: 100%;\n" +
      "      color: black;\n" +
      "      z-index: 200;\n" +
      "      background: rgba(0, 0, 0, 0.5) !important;\">\n" +
      "  <div style=\"position: relative;\n" +
      "      width: 70vw;\n" +
      "      margin: auto;\n" +
      "      top: 40vh;\n" +
      "      z-index: 250;\">\n" +
      "    <div style=\"border-radius: 100px;\n" +
      "      width: 100%;\n" +
      "      margin: auto;\n" +
      "      z-index: 300;\n" +
      "      text-align: center;\n" +
      "      display: flex;\n" +
      "      align-items: center;\n" +
      "      justify-content: center;\">\n" +
      "      <div style=\"width: 90%;\n" +
      "      border-radius: 3vw;\n" +
      "      background-color: white;\">\n" +
      "        <div id=\"modalText\"\n" +
      "             style=\"padding-left: 2vw;\n" +
      "          padding-right: 2vw;\n" +
      "          font-size: 4.5vw;\n" +
      "          padding-top: 6vw;\n" +
      "          padding-bottom: 4vw;\n" +
      "          border-bottom: 1px solid rgba(130,126,127,0.62)\">\n" +
      "          something\n" +
      "        </div>\n" +
      "        <div style=\"display: flex;\n" +
      "            justify-content: space-around;\n" +
      "            height: 10vw;\n" +
      "            align-items: center;\">\n" +
      "          <div style=\"display: flex;\n" +
      "              align-items: center;\n" +
      "              justify-content: center;\n" +
      "              width: 50%;height: 100%;\n" +
      "              border-right: 1px solid rgba(130,126,127,0.62)\">\n" +
      "            <div id=\"modelCancleBtn\" style=\"font-size: 4.5vw\">Cancle</div>\n" +
      "          </div>\n" +
      "          <div style=\"width: 50%;\n" +
      "            height: 100%;display: flex;\n" +
      "            align-items: center;\n" +
      "            justify-content: center;\">\n" +
      "            <div id=\"modelConfrimBtn\" style=\"font-size: 4.5vw\">Confirm</div>\n" +
      "          </div>\n" +
      "        </div>\n" +
      "      </div>\n" +
      "    </div>\n" +
      "  </div>\n" +
      "</div>");
  }
  var container = $('#model-div-container');
  container.data('attachData', JSON.stringify(attachData));

  $("#modelCancleBtn").unbind('click').click(function () {
    var container = document.getElementById('model-div-container');
    container.style.display = "none";
    cancle();
  })

  $("#modelConfrimBtn").unbind('click').click(function () {
    var container = $('#model-div-container');
    container.hide();
    var attachData = container.data('attachData');
    attachData = JSON.parse(attachData);
    ok(attachData);
  })

  if (content) {
    var modalText = document.getElementById('modalText');
    modalText.innerText = content;
  }
  var container = document.getElementById('model-div-container');
  container.style.display = "block";
}

/*toast弹框*/
function showToast(content, ttl) {

  var toastDiv = document.getElementById('toastDiv');
  if (!toastDiv) {
    $('body').append("<div id=\"toastDiv\" style=\"position: fixed;\n" +
      "  top: 70vh;\n" +
      "  display: none;\n" +
      "  width: 80%;\n" +
      "  margin-left: 10%;\n" +
      "  align-items: center;\n" +
      "  justify-content: center;\n" +
      "  z-index: 9998;\n" +
      "\">\n" +
      "  <div style=\"display: flex;\n" +
      "    align-items: center;\n" +
      "    justify-content: center;\n" +
      "    color: white;\n" +
      "    z-index: 9999;\n" +
      "  \">\n" +
      "    <div style=\"padding-left: 5vw;\n" +
      "      padding-right: 5vw;\n" +
      "      padding-top: 3vw;\n" +
      "      background: rgba(77, 77, 77, 0.9) !important;;\n" +
      "      border-radius: 2vw;\n" +
      "      font-size: 4vw;\n" +
      "      z-index: 10000;\n" +
      "      padding-bottom: 3vw;\" id=\"toastText\">\n" +
      "      toastText\n" +
      "    </div>\n" +
      "  </div>\n" +
      "</div>");
  }

  var toastText = document.getElementById('toastText');
  toastText.innerText = content;

  var toastDiv = document.getElementById('toastDiv');
  toastDiv.style.display = "block";
  setTimeout(function () {
    toastDiv.style.display = "none";
  }, ttl * 1000)
}

/**
 * 显示登陆窗口
 */
function showLoginModel(onLogin) {
  let loginDiv = document.getElementById('login-div-container');
  if (!loginDiv) {
    $('body').append("<div id=\"login-div-container\"\n" +
      "     style=\"display: none; position: fixed; top: 0px; width: 100%; height: 100%; z-index: 200; background: rgba(0, 0, 0, 0.5) !important;\">\n" +
      "  <div style=\"position: relative;\n" +
      "      width: 70vw;\n" +
      "      margin: auto;\n" +
      "      top: 40vh;\n" +
      "      z-index: 250;\">\n" +
      "    <div style=\"border-radius: 100px;\n" +
      "      width: 100%;\n" +
      "      margin: auto;\n" +
      "      z-index: 300;\n" +
      "      text-align: center;\n" +
      "      display: flex;\n" +
      "      align-items: center;\n" +
      "      justify-content: center;\">\n" +
      "      <div style=\"width: 90%;\n" +
      "      border-radius: 3vw;\n" +
      "      background-color: white;\">\n" +
      "        <div style=\"padding-left: 2vw;\n" +
      "          padding-right: 2vw;\n" +
      "          font-size: 4vw;\n" +
      "          padding-top: 2vw;\n" +
      "          padding-bottom: 1vw;\n" +
      "          border-bottom: 1px solid #dee2e6\">\n" +
      "          <img src=\"../img/loginUser.png\" width=\"20%\"/>\n" +
      "        </div>\n" +
      "\n" +
      "        <div style=\"display: flex;align-items: center;justify-content: center;\n" +
      "                 border-bottom: 1px solid #dee2e6;width: 100%\">\n" +
      "\n" +
      "          <input type=\"text\" id=\"instName\"\n" +
      "                 placeholder=\"input your ins name\"\n" +
      "                 style=\"background-color: lavender;\n" +
      "                   font-size:4vw;\n" +
      "                   padding-left: 4vw;\n" +
      "                   padding-right: 4vw;\n" +
      "                   padding-top: 2.5vw;\n" +
      "                   padding-bottom: 2.5vw;\n" +
      "                   margin-top: 2vw;\n" +
      "                   margin-bottom: 2vw;\n" +
      "                   margin-left: 30vw;\n" +
      "                   margin-right: 30vw;\n" +
      "                   border-radius: 6vw;\n" +
      "                   text-align: center;\n" +
      "                   color: rgb(12, 13, 53);\n" +
      "                   border: none\"\n" +
      "                 autocapitalize=\"off\" width=\"100%\" height=\"5vw\" autofocus autocorrect=\"off\">\n" +
      "        </div>\n" +
      "        <div style=\"display: flex;\n" +
      "            justify-content: center;\n" +
      "            align-items: center;\">\n" +
      "          <div id=\"loginBtn\" style=\"background-color: rgb(12, 13, 53);\n" +
      "                   font-size:4vw;\n" +
      "                   padding-left: 4vw;\n" +
      "                   padding-right: 4vw;\n" +
      "                   padding-top: 2.5vw;\n" +
      "                   padding-bottom: 2.5vw;\n" +
      "                   margin-top: 3vw;\n" +
      "                   margin-bottom: 3vw;\n" +
      "                   border-radius: 6vw;\n" +
      "                   text-align: center;\n" +
      "                   width: 40.5vw;\n" +
      "                   color: white;\n" +
      "                   border: none\";>\n" +
      "              Login\n" +
      "          </div>\n" +
      "        </div>\n" +
      "      </div>\n" +
      "    </div>\n" +
      "  </div>\n" +
      "</div>");

    $('#loginBtn').click(function () {
      var instName = $('#instName').val();
      onLogin(instName);
    });
  }
  loginDiv = $('#login-div-container');
  loginDiv.show();
}

/**
 * 关闭登陆窗口
 */
function closeLoginModel() {
  let loginDiv = $('#login-div-container');
  loginDiv.hide();
}
